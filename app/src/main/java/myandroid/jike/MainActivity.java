package myandroid.jike;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.gigamole.navigationtabbar.ntb.NavigationTabBar;

import java.util.ArrayList;
import java.util.List;

import myandroid.jike.Sqlite.DatabaseHelper;
import myandroid.jike.fragment.MineFragment;

public class MainActivity extends AppCompatActivity{

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private Context context;
    private List<Fragment> mFragmentList = new ArrayList<Fragment>();
    private ViewPager mViewPager;
    private FragmentPagerAdapter mAdapter;

    private List<String> attentionList = new ArrayList<String>();
    private DatabaseHelper databaseHelper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mViewPager = (ViewPager) findViewById(R.id.id_viewPage);
        initUI();
        databaseHelper = new DatabaseHelper(this);
        preferences = getSharedPreferences("MainActivity", Context.MODE_PRIVATE);
        //判断是不是首次登录，
        if (preferences.getBoolean("firstStart", true)) {
            //将登录标志位设置为false，下次登录时不在显示首次登录界面
            editor.putBoolean("firstStart", false);
            editor.commit();

            //获取用户关注信息
            Intent intent = getIntent();
            Bundle bundle = intent.getBundleExtra("bundle");
            attentionList = bundle.getStringArrayList("list");
            databaseHelper.insertData(attentionList);

        }else{
            attentionList = databaseHelper.getAttentionList();
        }

        Toast.makeText(this,attentionList.toString(),Toast.LENGTH_SHORT).show();
    }

    //viewPage适配器
    private void initUI() {

        mFragmentList.add(new MineFragment());
        mFragmentList.add(new MineFragment());
        mFragmentList.add(new MineFragment());
        mFragmentList.add(new MineFragment());

        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Override
            public int getCount()
            {
                return mFragmentList.size();
            }

            @Override
            public Fragment getItem(int position)
            {
                return mFragmentList.get(position);
            }
        };

        mViewPager.setAdapter(mAdapter);



        //底部导航栏的实现
        final String[] colors = getResources().getStringArray(R.array.default_preview);

        final NavigationTabBar navigationTabBar = (NavigationTabBar) findViewById(R.id.ntb_horizontal);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_first),
                        Color.parseColor(colors[0]))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_sixth))
                        .title("推荐")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_second),
                        Color.parseColor(colors[1]))
                       .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title("关注")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_third),
                        Color.parseColor(colors[2]))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_seventh))
                        .title("发现")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_fourth),
                        Color.parseColor(colors[3]))
                       .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title("我的")
                        .build()
        );

        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(mViewPager, 3);
        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                navigationTabBar.getModels().get(position).hideBadge();
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

        navigationTabBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            model.showBadge();
                        }
                    }, i * 100);
                }
            }
        }, 500);
    }




}
